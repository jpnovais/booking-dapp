const path = require('path');
const webpack = require('webpack');

module.exports = {
    mode: 'development',
    devtool: 'inline-source-map',
    entry: './src/index.js',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist'),
    },
    devServer: {
        contentBase: 'dist'
    },
    plugins: [
        new webpack.EnvironmentPlugin(['BOOKING_CONTRACT_ADDRESS'])
    ]
};