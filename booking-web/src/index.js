import {
    CONTRACT_ABI,
    CONTRACT_ADDRESS,
} from './config.js';
import {
    removeCanceledBookings
} from './utils.js';
import {
    Booking
} from './booking.js';
import * as ui from './uihelper.js';

let bookingApp;
// global variable to have callbacks for event listeners
let app = {};
let web3;
window.app = app;

async function startEthereum() {
    if (window.ethereum) {
        window.ethereum.on('accountsChanged', () => window.location.reload());
        window.ethereum.on('chainChanged', () => window.location.reload());
        web3 = new Web3(window.ethereum);
        return window.ethereum.enable().then(() => web3);
    } else {
        return Promise.reject(new Error('Browser does not support Ethereum. Please install Metmask extension.'));
    }
}

function startApp() {
    return startEthereum()
        .then((web3) => {
            let instance = new web3.eth.Contract(CONTRACT_ABI, CONTRACT_ADDRESS);
            bookingApp = new Booking(instance);
            return bookingApp.getRooms();
        })
        .then((roomIds) => {
            if (!roomIds) {
                ui.dislpayFeedbackError('Could not fetch rooms. Please check your Metamask wallet configurations');
            } else {
                ui.fillRoomsDropDown(roomIds);
            }
        })
        .then(refreshBookings)
        .catch(displayMessageAndBubbleUpError);
}

function refreshBookings() {
    /**
     * extremely inefficient, but effective (for PoC/demo purpose only)
     */
    return bookingApp.loadBookings()
        .then(removeCanceledBookings)
        .then((events) => {
            let accounts = events.reduce((acsSet, e) => {
                acsSet.add(e.user);
                return acsSet;
            }, new Set());
            return Promise.all([events, bookingApp.getAccountsAdmins(Array.from(accounts))]);
        })
        .then(([events, accountsAdmins]) => {
            events.sort((eA, eB) => eA.timestamp - eB.timestamp);
            return ui.buildBookingsViewModels(events, accountsAdmins, ethereum.selectedAddress);
        })
        .then(ui.displayBookings)
        .catch(displayMessageAndBubbleUpError);
}

app.cancelBooking = function cancelBooking(roomId, time) {
    bookingApp.cancelBooking(roomId, time, ethereum.selectedAddress)
        .then((_) => refreshBookings())
        .catch(displayMessageAndBubbleUpError);
}

app.bookRoom = function bookRoom(arg) {
    let roomId = document.getElementById('rooms-list').value;
    let bookingTime = new Date(document.getElementById('booking-time').value);
    roomId = parseInt(roomId);
    bookingApp.bookRoom(roomId, bookingTime, ethereum.selectedAddress)
        .then((_) => refreshBookings())
        .catch(displayMessageAndBubbleUpError);
}

function displayMessageAndBubbleUpError(error) {
    ui.dislpayFeedbackError((error.message || error).toString());
    throw error;
}

window.addEventListener('load', startApp);