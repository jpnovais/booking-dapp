export function mapEventToDomain(blockchainEvent) {
    let { event, returnValues } = blockchainEvent
    let timestamp = returnValues.timestampInSeconds * 1000;
    return {
        bookingId: `${returnValues.roomId}-${timestamp}`,
        event: event,
        roomId: returnValues.roomId,
        timestamp,
        time: new Date(timestamp),
        user: returnValues.user.toLowerCase(),
        toDisplyString: function() {
            return eventDisplayString(this);
        }
    };
}

export function removeCanceledBookings(bookingEvents) {
    let cancelationsMap = bookingEvents
        .filter(e => e.event == 'RoomBookingCanceled')
        .reduce((acc, e) => {
            acc[e.bookingId] = true;
            return acc;
        }, {});
    return bookingEvents
        .filter(e => e.event == 'RoomBooked')
        .filter(e => !cancelationsMap[e.bookingId]);
}