import RoomBooking from '../../booking-dapp/build/contracts/RoomBooking.json';

export const CONTRACT_ABI = RoomBooking.abi;
// webpack will inject this at compile time
export const CONTRACT_ADDRESS = process.env.BOOKING_CONTRACT_ADDRESS;
export const ROOMS_LIST = [
    'C01',
    'C02',
    'C03',
    'C04',
    'C05',
    'C06',
    'C07',
    'C08',
    'C09',
    'C10',
    'P01',
    'P02',
    'P03',
    'P04',
    'P05',
    'P06',
    'P07',
    'P08',
    'P09',
    'P10',
];

export const ROOMS = ROOMS_LIST.reduce((mapAcc, room, roomIndex) => {
    mapAcc[room] = roomIndex;
    mapAcc[roomIndex] = room;
    return mapAcc;
}, {});