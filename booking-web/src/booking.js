import { mapEventToDomain } from './utils';

/**
 * Helper Class to intereact with Booking Smart contract
 */
export class Booking {
    constructor(bookingDApp) {
        this.dapp = bookingDApp;
        this.bookingListeners;
    }

    getRooms() {
        if (this.rooms === undefined) {
            // this can be cached as for the time being for this PoC
            // the rooms list is static
            return this.dapp.methods.listRooms().call()
                .then((roomsIds) => {
                    this.rooms = roomsIds;
                    return roomsIds;
                });
        } else {
            return Promise.resolve(this.rooms);
        }
    }

    getAccountsAdmins(accountIds) {
        return Promise
            .all(accountIds.map(account => this.dapp.methods.accountToAdminMap(account).call()))
            .then((admins) => {
                return accountIds.reduce((acc, account, index) => {
                    acc[account] = admins[index].toLowerCase();
                    return acc;
                }, {});
            });
    }

    loadBookings() {
        return this.dapp.getPastEvents('allEvents', {
                fromBlock: 0,
                toBlock: 'latest'
            })
            .then((events) => events.filter(({ event }) => event == 'RoomBooked' || event == 'RoomBookingCanceled'))
            .then((events) => events.map(mapEventToDomain));
    }

    bookRoom(roomId, timestamp, account = ethereum.selectedAddress) {
        return this.dapp.methods.bookRoom(roomId, timestamp / 1000)
            .send({ from: account });
    }

    cancelBooking(roomId, timestamp, account = ethereum.selectedAddress) {
        return this.dapp.methods.cancelBooking(roomId, timestamp / 1000)
            .send({ from: account });
    }

    onRoomBooked(callback) {
        this.dapp.events.RoomBooked(callback);
    }

    onRoomBookingCanceled(callback) {
        this.dapp.events.onRoomBookingCanceled(callback);
    }
}