import { ROOMS } from './config.js';

export function setNextHour() {
    let nextHour = new Date();
    nextHour.setHours(nextHour.getHours() + 1);
    nextHour.setMinutes(0);
    nextHour.setSeconds(0);
    nextHour.setMilliseconds(0);
    $('#booking-time').val(nextHour.toISOString().substring(0, 16));
}

export function dislpayFeedbackError(message, elem = $('#error-feedback')) {
    elem.append(`<p>${message}</p>`);
}

export function fillRoomsDropDown(roomsIds = [], elem = $('#rooms-list')) {
    roomsIds.forEach((roomId) => {
        if (roomId == 0) {
            elem.append(`<option value="${roomId}" label="${ROOMS[roomId]}" selected="selected"></option>`);
        } else {
            elem.append(`<option value="${roomId}" label="${ROOMS[roomId]}"></option>`);
        }
    });
}

function eventDisplayString(event) {
    return `${event.event}(${new Date(event.timestamp).toISOString().substring(0, 16)}, ${event.roomName}, ${event.user.substring(0, 10)}...)`;

}

export function displayBookings(bookingsList, selectedAddress = ethereum.selectedAddress, elem = $('#bookings-list')) {
    selectedAddress = selectedAddress.toLowerCase();
    elem.empty();
    bookingsList
        .forEach(booking =>
            displayBooking(booking, elem)
        );
};

export function displayBooking(booking, elem = $('#bookings-list')) {
    let div = $(`<div></div>`)
        .append(`<label>${eventDisplayString(booking)}</label>`);
    if (booking.allowCancel) {
        div.append(`
		<button 
			type="button" 
			value="${booking.roomId}-${booking.time}" 
			onclick="app.cancelBooking(${booking.roomId},${booking.timestamp});">
			Cancel</button>`);
    }
    elem.append(div);
}

export function buildBookingViewModel(event, accountsAdmins, selectedAddress) {
    selectedAddress = selectedAddress.toLowerCase();
    let allowCancel = event.user == selectedAddress || (accountsAdmins[event.user] || '') == selectedAddress;
    return {
        roomId: event.roomId,
        timestamp: event.timestamp,
        roomName: ROOMS[event.roomId],
        event: event.event,
        user: event.user,
        allowCancel: allowCancel,
    };
}
export function buildBookingsViewModels(events, accountsAdmins, selectedAddress) {
    return events.map(e => buildBookingViewModel(e, accountsAdmins, selectedAddress));
}