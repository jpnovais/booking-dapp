# Room Booking Dapp

In order to address the challenge requirements a Smart Contract was developed (my first 🙂) .
The contract owner can add **admin** accounts, which can authorize other accounts to use the booking application. Admin accounts are allowed to cancel bookings made by the users/accounts they own. Regular user's can only cancel their own bookings.
For purpose of this assignment, only key features were developed in the Booking app, leaving aside things like full user/account management, etc.

**Note** In development/demo the migration script automatically adds 2 admins (1 for Cola, 1 Pepsi) and authorizes 2 other accounts, associated with each admin. 

## Requirements
- NodeJS (used v14.15.4)
- ganache-cli (used Ganache CLI v6.12.2 (ganache-core: 2.13.2))
- truffle (used v5.1.64)
- Metamask browser extension

## Running instructions
1. start test blockchain with ganache cli.
```bash
ganache-cli -d -m "found puppy resemble account bubble trend cream know mercy safe company enable" 
```

2. Deploy booking dapp 
```bash
cd booking-dapp
npm install
truffle deploy --network development
```

The deployment to `development` blockchain will automatically setup and authorize a few accounts to use the `RoomBooking` smart contract. The **contract address will be necessary** on step 4.

If successful, should print something like:
```
Deploying 'RoomBooking'
   -----------------------
   > transaction hash:    0x7a2f3e804929533abe741fca80be52d80a14aa429b3fafc14307f36e74830f1c
   > Blocks: 0            Seconds: 0
   > contract address:    0xfF7E7D489a62F9F0300FD6B0F9ee9d4FBa3A4A73
   > block number:        5
   > block timestamp:     1612896239
   > account:             0x606b234A1080d80CD5C2232B64Cd64626A1A4383
   > balance:             99.95682286
   > gas used:            1631093 (0x18e375)
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.03262186 ETH

Developmnet accounts:
Contract Owner: 0x606b234A1080d80CD5C2232B64Cd64626A1A4383
Cola
  0x8B354c58831C5Fc371B038760C6199624983E3cf (admin)
  0xA4a3a1CfD8ACbF5E12444fB0d239C391b4c71da8
  0x986322be9b20D398cFb88D75B8Af4FDe344BF393
Pepsi
  0x70F67fACD02E04bf012D91df95F3760Fd56658a5 (admin)
  0xf8B0bc09B92f7B85480377b34c86D8A338979Fde
  0x3Ea4C4E8Cf067BC5bFFEd268fd98c2D9E2cB6b29
Unauthorized
  0xdE31CFa51757bc783E73B68754926C42a149B5a4
  0x1fcA9Bb6F17E0ebdE6E17903023EE5Ccba9FA87e
  0xac30860a030dD7af4d4F7C140B8976B5eD55cA9e

   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.03859118 ETH
```
Take note of the contra

3. Setup Metamask
- Add local networt to Metamask `http://127.0.0.1:8545`
- Import test accounts using **"Import using account seed phrase"**. Seed phrase is 
```
found puppy resemble account bubble trend cream know mercy safe company enable
```

**Note**: Account1 is not allowed not make bookings. So it's necessary to generate more accounts: 2..7, account 2 and 3 are admins which can cancel their user's boookings.

4. Start webpack to launch PoC web app.
```
cd booking-web
npm install
BOOKING_CONTRACT_ADDRESS=<contract address from step to> npm run demo
```
Open your browser on [http://localhost:8080/](http://localhost:8080/)

Sorry for my poor CSS/FED skills :(
