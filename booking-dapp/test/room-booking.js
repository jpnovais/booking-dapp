const RoomBooking = artifacts.require("RoomBooking");
const expect = require('chai').expect;
const truffleAssert = require('truffle-assertions');

contract('RoomBooking', (accounts) => {
    let [owner, org1AdminAccount, org2AdminAccount, org1RegAccount1, org1RegAccount2, org2RegAccount1, org2RegAccount2] = accounts;
    let bookingApp;

    async function assertRoomIsBooked(roomId, expectedBookingTimestampInSeconds, expectedBookingOwner) {
        const bookingOwner = await bookingApp.bookings.call(roomId, expectedBookingTimestampInSeconds);
        expect(bookingOwner).to.be.eql(expectedBookingOwner);
    }

    async function assertRoomIsFree(roomId, bookingTimestampInSeconds) {
        const bookingOwner = await bookingApp.bookings.call(roomId, bookingTimestampInSeconds);
        expect(bookingOwner).to.be.eql('0x0000000000000000000000000000000000000000');
    }


    beforeEach(async() => {
        bookingApp = await RoomBooking.new([1, 2, 3, 4, 5, 6], { from: owner });
    });

    it('should create contrat with initial rooms', async() => {
        let rooms = (await bookingApp.listRooms())
            .map(roomBN => roomBN.toNumber());

        expect(rooms).to.be.eql([1, 2, 3, 4, 5, 6]);
    });

    context('bookings', async() => {
        beforeEach(async() => {
            await bookingApp.addAdminAccount(org1AdminAccount, { from: owner });
            await bookingApp.addAdminAccount(org2AdminAccount, { from: owner });
            await bookingApp.authorizeAccount(org1RegAccount1, { from: org1AdminAccount });
            await bookingApp.authorizeAccount(org2RegAccount1, { from: org2AdminAccount });
        });

        it('should reject bookings from non-authorized users', async() => {
            const bookingTimestamp = new Date("2021-02-04T11:00:00").valueOf() / 1000;
            await truffleAssert.reverts(
                bookingApp.bookRoom(1, bookingTimestamp, { from: org2RegAccount2 }),
                'Account not authorized.',
                'Should have rejected transaction because user is not admin.'
            )
        });

        it('should reject bookings that are not by the hour', async() => {
            const bookingTimestamp = new Date("2021-02-04T11:15:23").valueOf();
            await truffleAssert.reverts(
                bookingApp.bookRoom(1, bookingTimestamp, { from: org1RegAccount1 }),
                'Invalid booking time: it must be an exact hour.',
                'Should have reject transaction. Timestamp does not represent a valid time.'
            );
        });

        it('should allow user to book free room', async() => {
            const bookingTimestamp = new Date("2021-02-04T11:00:00").valueOf() / 1000;
            const receipt = await bookingApp.bookRoom(1, bookingTimestamp, { from: org1RegAccount1 });

            await assertRoomIsBooked(1, bookingTimestamp, org1RegAccount1);
            // check event
            truffleAssert.eventEmitted(receipt, 'RoomBooked', (ev) => {
                expect(ev.roomId.toNumber()).to.be.equal(1, 'Wrong roomId')
                expect(ev.timestampInSeconds.toNumber()).to.be.equal(bookingTimestamp, 'Wrong time');
                expect(ev.user).to.be.equal(org1RegAccount1, 'Wrong user');
                return true;
            });
        });

        it('should reject already booked room', async() => {
            const bookingTimestamp = new Date("2021-02-04T11:00:00").valueOf() / 1000;
            await bookingApp.bookRoom(1, bookingTimestamp, { from: org1RegAccount1 });
            await truffleAssert.reverts(
                bookingApp.bookRoom(1, bookingTimestamp, { from: org2RegAccount1 }),
                'Room is already booked at given time.',
                'Should have reject transaction. RoomId was already booked for the same time'
            );
        });

        it('should reject user trying to book non existing room', async() => {
            const bookingTimestamp = new Date("2021-02-04T11:00:00").valueOf() / 1000;
            await truffleAssert.reverts(
                bookingApp.bookRoom(11, bookingTimestamp, { from: org2RegAccount1 }),
                'Room does not exist.',
                'Should have reject transaction. RoomId does not exist'
            );
        });

        it('should allow user to cancel his own reservation', async() => {
            const bookingTimestamp = new Date("2021-02-04T11:00:00").valueOf() / 1000;
            await bookingApp.bookRoom(1, bookingTimestamp, { from: org1RegAccount1 });
            // assert it was removed
            const receipt = await bookingApp.cancelBooking(1, bookingTimestamp, { from: org1RegAccount1 });
            await assertRoomIsFree(1, bookingTimestamp);

            truffleAssert.eventEmitted(receipt, 'RoomBookingCanceled', (ev) => {
                expect(ev.roomId.toNumber()).to.be.equal(1, 'Wrong roomId')
                expect(ev.timestampInSeconds.toNumber()).to.be.equal(bookingTimestamp, 'Wrong time');
                expect(ev.user).to.be.equal(org1RegAccount1, 'Wrong user');
                return true;
            });
        });

        it('should allow canceled time slots to be rebooked again', async() => {
            const bookingTimestamp = new Date("2021-02-04T11:00:00").valueOf() / 1000;
            await bookingApp.bookRoom(1, bookingTimestamp, { from: org1RegAccount1 });
            await bookingApp.cancelBooking(1, bookingTimestamp, { from: org1RegAccount1 });

            const receipt = await bookingApp.bookRoom(1, bookingTimestamp, { from: org2RegAccount1 });
            await assertRoomIsBooked(1, bookingTimestamp, org2RegAccount1);

            truffleAssert.eventEmitted(receipt, 'RoomBooked', (ev) => {
                expect(ev.roomId.toNumber()).to.be.equal(1, 'Wrong roomId')
                expect(ev.timestampInSeconds.toNumber()).to.be.equal(bookingTimestamp, 'Wrong time');
                expect(ev.user).to.be.equal(org2RegAccount1, 'Wrong user');
                return true;
            });
        });

        it('should reject cancelations that are not from booking owner', async() => {
            const bookingTimestamp = new Date("2021-02-04T11:00:00").valueOf() / 1000;
            await bookingApp.bookRoom(1, bookingTimestamp, { from: org1RegAccount1 });
            await truffleAssert.reverts(
                bookingApp.cancelBooking(1, bookingTimestamp, { from: org2RegAccount1 }),
                'Account not authorized: caller must be booking owner.',
                'Should have reject transaction. User should be allowed to cancel other users bookings.'
            );
        });

        it('should allow admin to cancel it\'s owned accounts reservations', async() => {
            const bookingTimestamp = new Date("2021-02-04T11:00:00").valueOf() / 1000;
            await bookingApp.bookRoom(1, bookingTimestamp, { from: org1RegAccount1 });

            const receipt = await bookingApp.cancelBooking(1, bookingTimestamp, { from: org1AdminAccount });
            await assertRoomIsFree(1, bookingTimestamp);

            truffleAssert.eventEmitted(receipt, 'RoomBookingCanceled', (ev) => {
                expect(ev.roomId.toNumber()).to.be.equal(1, 'Wrong roomId')
                expect(ev.timestampInSeconds.toNumber()).to.be.equal(bookingTimestamp, 'Wrong time');
                expect(ev.user).to.be.equal(org1AdminAccount, 'Wrong user');
                return true;
            });
        });
    });
});