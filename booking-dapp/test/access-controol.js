const AccessControll = artifacts.require("AccessControll");
const expect = require('chai').expect;
const truffleAssert = require('truffle-assertions');

contract('AccessControll', (accounts) => {
    let [owner, org1AdminAccount, org2AdminAccount, org1RegAccount1, org1RegAccount2, org2RegAccount1] = accounts;
    let accessControll;

    beforeEach(async() => {
        accessControll = await AccessControll.new({ from: owner });
    });

    it('should allow onwer to add new addmins', async() => {
        await accessControll.addAdminAccount(org1AdminAccount, { from: owner });
        expect((await accessControll.admins.call(org1AdminAccount))).to.be.eql(true);
    });

    it('should not allow addmin to add other addmins', async() => {
        await accessControll.addAdminAccount(org1AdminAccount, { from: owner });

        await truffleAssert.reverts(
            accessControll.addAdminAccount(org2AdminAccount, { from: org1AdminAccount }),
            'Ownable: caller is not the owner.',
            'Should have reject transaction. Not wonner calling'
        );
    });

    it('should allow admin authorize regullar accounts', async() => {
        await accessControll.addAdminAccount(org1AdminAccount, { from: owner });
        await accessControll.authorizeAccount(org1RegAccount1, { from: org1AdminAccount });

        expect((await accessControll.authorizedAccounts.call(org1RegAccount1))).to.be.eql(true);
        expect((await accessControll.accountToAdminMap.call(org1RegAccount1))).to.be.eql(org1AdminAccount);
    });

    it('should not allow regular accounts add other addmins', async() => {
        await accessControll.addAdminAccount(org1AdminAccount, { from: owner });
        await accessControll.authorizeAccount(org1RegAccount1, { from: org1AdminAccount });

        await truffleAssert.reverts(
            accessControll.addAdminAccount(org1RegAccount2, { from: org1RegAccount1 }),
            'Ownable: caller is not the owner.',
            'Should have reject transaction. Not right admin calling'
        );
    });

    it('should not allow regullar accounts to authorize other accounts', async() => {
        await accessControll.addAdminAccount(org1AdminAccount, { from: owner });
        await accessControll.authorizeAccount(org1RegAccount1, { from: org1AdminAccount });

        await truffleAssert.reverts(
            accessControll.authorizeAccount(org1RegAccount2, { from: org1RegAccount1 }),
            'Account not authorized: caller must be admin.',
            'Should have reject transaction. Not admin calling'
        );
    });

    it('should allow admin account to revoque authorization of his previously authorized accounts', async() => {
        await accessControll.addAdminAccount(org1AdminAccount, { from: owner });
        await accessControll.authorizeAccount(org1RegAccount1, { from: org1AdminAccount });
        await accessControll.removeAuthorizedAccount(org1RegAccount1, { from: org1AdminAccount });

        expect((await accessControll.authorizedAccounts.call(org1RegAccount1))).to.be.eql(false);
        expect((await accessControll.accountToAdminMap.call(org1RegAccount1)))
            .to.be.eql('0x0000000000000000000000000000000000000000');
    });

    it('should not allow admin account to revoque authorization of other admins\' accounts', async() => {
        await accessControll.addAdminAccount(org1AdminAccount, { from: owner });
        await accessControll.addAdminAccount(org2AdminAccount);
        await accessControll.authorizeAccount(org1RegAccount1, { from: org1AdminAccount });
        await truffleAssert.reverts(
            accessControll.removeAuthorizedAccount(org1RegAccount1, { from: org2AdminAccount }),
            'Account not authorized: caller must be admin of given account.',
            'Should have reject transaction. Not right admin calling'
        );
    });

    it('should not allow admins to override already authorized accouts', async() => {
        await accessControll.addAdminAccount(org1AdminAccount, { from: owner });
        await accessControll.addAdminAccount(org2AdminAccount);
        await accessControll.authorizeAccount(org1RegAccount1, { from: org1AdminAccount });
        await truffleAssert.reverts(
            accessControll.authorizeAccount(org1RegAccount1, { from: org2AdminAccount }),
            'Account is already authorized.',
            'Should have reject transaction. Not right admin calling'
        );
    });

    it('should tell if caller is admin of given account', async() => {
        await accessControll.addAdminAccount(org1AdminAccount, { from: owner });
        await accessControll.authorizeAccount(org1RegAccount1, { from: org1AdminAccount });
        expect((await accessControll.amIAdminOf(org1RegAccount1, { from: org1AdminAccount })))
            .to.be.eql(true);
    });
});