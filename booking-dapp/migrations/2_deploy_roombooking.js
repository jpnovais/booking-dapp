const BokkyPooBahsDateTimeLibrary = artifacts.require("BokkyPooBahsDateTimeLibrary");
const TimeLib = artifacts.require("TimeLib");
// const AccessControll = artifacts.require("AccessControll");
const RoomBooking = artifacts.require("RoomBooking");
// 0..9 Cola Rooms
// 10..19 Pepsi Rooms
const ROOMS_LIST = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19];

async function setupDevelopementAccounts(bookingApp, accounts) {
    let [owner, adminCola, adminPepsi, colaU1, colaU2, pepsiU1, pepsiU2] = accounts;

    return bookingApp.addAdminAccount(adminCola, { from: owner })
        .then(() => bookingApp.addAdminAccount(adminPepsi, { from: owner }))
        .then(() => {
            bookingApp.authorizeAccount(colaU1, { from: adminCola });
            bookingApp.authorizeAccount(colaU2, { from: adminCola });
            bookingApp.authorizeAccount(pepsiU1, { from: adminPepsi });
            bookingApp.authorizeAccount(pepsiU2, { from: adminPepsi });
            console.log('Developmnet accounts:');
            console.log(`Contract Owner: ${owner}`);
            console.log(`Cola`);
            console.log(`  ${adminCola} (admin)`);
            [colaU1, colaU2].forEach(account => console.log(`  ${account}`));
            console.log(`Pepsi`);
            console.log(`  ${adminPepsi} (admin)`);
            [pepsiU1, pepsiU2].forEach(account => console.log(`  ${account}`));

            console.log(`Unauthorized`);
            let unauthorized = accounts.splice(7);
            unauthorized.forEach(account => console.log(`  ${account}`));
        });
}

module.exports = async function(deployer, network, accounts) {
    deployer.deploy(BokkyPooBahsDateTimeLibrary);
    deployer.link(BokkyPooBahsDateTimeLibrary, TimeLib);
    deployer.deploy(TimeLib);
    deployer.link(TimeLib, RoomBooking);
    return deployer.deploy(RoomBooking, ROOMS_LIST)
        .then((_) => {
            if (network == 'development') {
                return RoomBooking.deployed()
                    .then((bookingApp) => setupDevelopementAccounts(bookingApp, accounts));
            }
        });
};