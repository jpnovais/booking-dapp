// SPDX-License-Identifier: MIT
pragma solidity >=0.5.16 <0.7.0;

import "./BokkyPooBahsDateTimeLibrary/BokkyPooBahsDateTimeLibrary.sol";

library TimeLib {
	function truncateTimeToHour(uint timestampInSeconds) public pure returns (uint) {
		uint year;
		uint month;
		uint day;
		uint hour;
		uint minute;
		uint second;
		(year, month, day, hour, minute, second) = BokkyPooBahsDateTimeLibrary.timestampToDateTime(timestampInSeconds);
		return BokkyPooBahsDateTimeLibrary.timestampFromDateTime(year, month, day, hour, 0, 0);
	}
}

