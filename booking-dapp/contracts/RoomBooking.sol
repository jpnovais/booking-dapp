// SPDX-License-Identifier: MIT
pragma solidity >=0.5.16 <0.7.0;

import "./TimeLib.sol";
import "./AccessControll.sol";

contract RoomBooking is AccessControll {
	uint8[] public rooms; //TODO: check for possible overflows
	// mapping(address => bool) public admins;
	// mapping(address => bool) public users;
	// mapping (address => address) public userToAdminMap;
	// {roomId -> {dateTime -> user}} 
	mapping (uint8 => mapping (uint => address)) public bookings;

	event RoomBooked(uint8 roomId, uint timestampInSeconds, address user);
	event RoomBookingCanceled(uint8 roomId, uint timestampInSeconds, address user);

	constructor(uint8[] memory _initialRooms) public {
		rooms = _initialRooms;
	}

	modifier roomExists(uint8 _roomId) {
		bool roomFound = false;
		for (uint256 i = 0; i < rooms.length; i++) {
			if (rooms[i] == _roomId) {
				roomFound = true;
				break;
			}
		}
		require(roomFound, 'Room does not exist.');
		_;
	}

	modifier _isValidTime(uint _timestampInSeconds) {
		require(_timestampInSeconds == TimeLib.truncateTimeToHour(_timestampInSeconds), 'Invalid booking time: it must be an exact hour.');
		_;
	}

	function listRooms() public view returns (uint8[] memory) {
		return rooms;
	}

	function bookRoom(uint8 roomId, uint timestampInSeconds) 
	public isAuthorized _isValidTime(timestampInSeconds) roomExists(roomId) 
	{
		require(bookings[roomId][timestampInSeconds] == address(0), 'Room is already booked at given time.');
		bookings[roomId][timestampInSeconds] = msg.sender;
		emit RoomBooked(roomId, timestampInSeconds, msg.sender);
	}

	function _isAuthorizedToCancel(uint8 roomId, uint bookingTime) internal view returns(bool) {
		address bookingOwner = bookings[roomId][bookingTime];
		return (msg.sender == bookingOwner || amIAdminOf(bookingOwner));
	}

	function cancelBooking(uint8 roomId, uint timestampInSeconds) 
	public isAuthorized _isValidTime(timestampInSeconds) roomExists(roomId) 
	{
		require(_isAuthorizedToCancel(roomId, timestampInSeconds), 'Account not authorized: caller must be booking owner.');
		delete(bookings[roomId][timestampInSeconds]);
		emit RoomBookingCanceled(roomId, timestampInSeconds, msg.sender);
	}
}
