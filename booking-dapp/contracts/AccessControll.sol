// SPDX-License-Identifier: MIT
pragma solidity >=0.5.16 <0.7.0;

import "@openzeppelin/contracts/access/Ownable.sol";

contract AccessControll is Ownable {
	mapping(address => bool) public admins;
	mapping(address => bool) public authorizedAccounts;
	mapping(address => address) public accountToAdminMap;

	// event AdminAdded(address admin);
	// event AccountAuthorized(address account, address indexed addmin);

	modifier isAdminAccount() {
		require(admins[msg.sender], 'Account not authorized: caller must be admin.');
		_;
	}

	modifier isAuthorized() {
		require(authorizedAccounts[msg.sender] || admins[msg.sender], 'Account not authorized.');
		_;
	}

	function amIAdminOf(address account) public view returns (bool) {
		return (accountToAdminMap[account] == msg.sender);
	}

	function addAdminAccount(address newAdmin) public onlyOwner {
		admins[newAdmin] = true;
	}

	function authorizeAccount(address newAccount) public isAdminAccount {
		require(accountToAdminMap[newAccount] == address(0), 'Account is already authorized.');
		authorizedAccounts[newAccount] = true;
		accountToAdminMap[newAccount] = msg.sender;
	}

	function removeAuthorizedAccount(address account) public isAdminAccount {
		require(amIAdminOf(account), 'Account not authorized: caller must be admin of given account.');
		delete(authorizedAccounts[account]);
		delete(accountToAdminMap[account]);
	}
}
